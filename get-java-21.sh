#!/bin/bash

set -euo pipefail

dir=${1:-~/java}

test -d $dir || exit 3

MAJOR=21
VERSION="${MAJOR}.0.1"
BUILD=12

test -x $dir/jdk-${VERSION}/bin/java || \
     curl -fjksSL "https://download.bell-sw.com/java/${VERSION}+${BUILD}/bellsoft-jdk${VERSION}+${BUILD}-linux-amd64.tar.gz" \
    | tar -xzf - -C $dir
rm -f $dir/jdk-${MAJOR}; ln -s jdk-${VERSION} $dir/jdk-${MAJOR}
rm -f $dir/jdk;  ln -s jdk-${MAJOR} $dir/jdk
