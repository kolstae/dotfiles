#!/bin/sh

echo "Installing dependencies"
sudo apt-get install vim git zsh postgresql
# sudo apt-get install i3 emacs adobe-sourcecodepro-fonts adobe-sourcesanspro-fonts adobe-sourceserifpro-fonts

echo

echo "Creating folders"
[ -d ~/projects ] || mkdir ~/projects
[ -d ~/java ] || mkdir ~/java
[ -d ~/bin ] || mkdir ~/bin

### git

echo "Configuring git"
echo "Which email address do you want to use?"
read email
git config --global user.name "Espen Amble Kolstad"
git config --global user.email "$email"
git config --global alias.lol "log --graph --decorate --pretty=oneline --abbrev-commit"
git config --global alias.lola "log --graph --decorate --pretty=oneline --abbrev-commit --all"
git config --global alias.hist "log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short"
git config --global alias.co "checkout"
git config --global alias.br "branch"
git config --global alias.st "status"
git config --global push.default current
git config --global pull.rebase true
git config --global branch.autosetuprebase always
git config --global branch.autosetupmerge always

echo

echo "Configuring shell (zsh)"
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s /usr/bin/zsh
cp zsh/zshrc ~/.zshrc
cp zsh/zshenv ~/.zshenv

echo "PROMPT='%{\$fg[red]%}%n%{\$fg[white]%}@%{\$fg_bold[green]%}%m %{\$fg[cyan]%}%~ %{\$fg_bold[blue]%}\$(git_prompt_info)%{\$fg_bold[blue]%} % %{\$reset_color%}'" >> ~/.zshrc

echo

echo "Configuring psql"
cp psql/psqlrc ~/.psqlrc

echo

echo "Configuring java"
./get-java.sh
curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > ~/bin/lein && chmod u+x ~/bin/lein

echo

# echo "Configuring emacs"
# git clone https://github.com/kolstae/.emacs.d.git ̃/.emacs.d

# echo "Configuring i3"
# mkdir ~/.i3
# [ -f ~/.i3/config ] || cp i3/config/config ~/.i3/config
# cp i3/i3exit ~/bin/
# cp i3/i3status.conf ~/.i3status.conf
# xdg-mime default pcmanfm.desktop inode/directory

# echo
# echo

echo "ssh:     Generate keys (ssh-keygen -t rsa -b 4096 -C \"$email\")"
